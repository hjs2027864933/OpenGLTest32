﻿#pragma execution_character_set("utf-8")
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <SOIL2\soil2.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cmath>
#include <stack>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp> // glm::value_ptr
#include <glm\gtc\matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include "Utils.h"
#include "Sphere.h"
#include "camera.h"

using namespace std;

// callback

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void window_size_callback(GLFWwindow* win, int newWidth, int newHeight);
void processInput(GLFWwindow* window);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);

const int VaoNum = 1;
const int VboNum = 2;
const int SunTextureFileNum = 3;
const int EarthTextureFileNum = 3;
const GLfloat PI = 3.14159265358979323846f;
int SunTextureIndex = 0;
int EarthTextureIndex = 0;
int width=800, height=600;

GLuint Vao[VaoNum];
GLuint Vbo[VaoNum];
GLuint LightVao;
GLuint EarthTexture;
GLuint SunTexture;
GLuint renderingProgram; //着色器

string SunTextureFileNames[SunTextureFileNum] = { "res/sun.jpg","res/deng.jpg","res/shuixing.jpg"};
string EarthTextureFileNames[EarthTextureFileNum] = { "res/earth.jpg","res/muqiu.jpg","res/qiu.jpg" };

// variable allocation for display
GLuint mvLoc, projLoc;
stack<glm::mat4> mvStack;
glm::mat4 pMat, vMat, mMat, mvMat;


// camera
Camera camera(glm::vec3(0.0f, 0.0f, 4.0f));
float lastX = width / 2.0f;
float lastY = height / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f; // 当前帧与上一帧的时间差
float lastFrame = 0.0f; // 上一帧的时间




Sphere mySphere = Sphere(60);
std::vector<int> sphereIndices;

void setupVertices(void) {

    std::vector<int> ind = mySphere.getIndices();
    std::vector<glm::vec3> vert = mySphere.getVertices();
    std::vector<glm::vec2> tex = mySphere.getTexCoords();
    std::vector<float> pvalues;	//顶点坐标
    std::vector<float> tvalues;	//纹理坐标

    int numIndices = mySphere.getNumIndices();
    for (int i = 0; i < numIndices; i++) {
        //顶点坐标
        pvalues.push_back((vert[ind[i]]).x);
        pvalues.push_back((vert[ind[i]]).y);
        pvalues.push_back((vert[ind[i]]).z);
        //纹理坐标
        tvalues.push_back((tex[ind[i]]).s);
        tvalues.push_back((tex[ind[i]]).t);
    }

    glGenVertexArrays(VaoNum, Vao);
    glGenVertexArrays(1, &LightVao);
    glGenBuffers(VboNum, Vbo);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo[0]);
    glBufferData(GL_ARRAY_BUFFER, pvalues.size() * 4, &pvalues[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, Vbo[1]);
    glBufferData(GL_ARRAY_BUFFER, tvalues.size() * 4, &tvalues[0], GL_STATIC_DRAW);

}
//创建并加载纹理
void creatTexture(GLuint &Texture, string texturefilename)
{
    glBindTexture(GL_TEXTURE_2D, Texture);
    int widthTexture, heightTexture;
    unsigned char* image = SOIL_load_image(texturefilename.c_str(), &widthTexture, &heightTexture, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, widthTexture, heightTexture, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0);
}
void init(GLFWwindow* window) {

    renderingProgram = Utils::createShaderProgram("vertShader.vs", "fragShader.fs");
    pMat = glm::perspective(glm::radians(camera.Zoom), (float)width / (float)height, 0.1f, 1000.0f);
    setupVertices();

    //配置太阳模型
    glGenTextures(1, &SunTexture);
    creatTexture(SunTexture, SunTextureFileNames[SunTextureIndex]);

    glBindVertexArray(LightVao);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo[1]);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);

    //配置地球模型
    glGenTextures(1, &EarthTexture);
    creatTexture(EarthTexture, EarthTextureFileNames[EarthTextureIndex]);

    glBindVertexArray(Vao[0]);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo[0]);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, Vbo[1]);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);
    glBindVertexArray(0);

}

void display(GLFWwindow* window, double currentTime) {
    // 帧时间差
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // 输入
    processInput(window);

    glClear(GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    //激活着色器
    glUseProgram(renderingProgram);

    mvLoc = glGetUniformLocation(renderingProgram, "mv_matrix");
    projLoc = glGetUniformLocation(renderingProgram, "proj_matrix");

    pMat = glm::perspective(glm::radians(camera.Zoom), (float)width / (float)height, 0.1f, 1000.0f);

    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(pMat));

    vMat = camera.GetViewMatrix();
    mvStack.push(vMat);


    //Sun 1/4大小 且自转
    mvStack.push(mvStack.top());
    mvStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
    mvStack.top() *= rotate(glm::mat4(1.0f), (float)currentTime, glm::vec3(0.0, 1.0, 0.0));
    mvStack.top() *= scale(glm::mat4(1.0f), glm::vec3(0.25f, 0.25f, 0.25f));
    glUniformMatrix4fv(mvLoc, 1, GL_FALSE, glm::value_ptr(mvStack.top()));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(pMat));

    //调整深度
    glEnable(GL_CULL_FACE);		
    glFrontFace(GL_CCW);


    glBindVertexArray(LightVao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, SunTexture);
    glDrawArrays(GL_TRIANGLES, 0, mySphere.getNumIndices());
    glBindVertexArray(0);

    mvStack.pop();

    //Earth 1/10大小 自转且公转

    mvStack.push(mvStack.top());
    mvStack.top() *= glm::translate(glm::mat4(1.0f), glm::vec3(sin((float)currentTime) * 1.0, 0.0f, cos((float)currentTime) * 1.0));
    mvStack.top() *= rotate(glm::mat4(1.0f), (float)currentTime * 3, glm::vec3(0.0, 1.0, 0.0));
    mvStack.top() *= scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f));

    glUniformMatrix4fv(mvLoc, 1, GL_FALSE, glm::value_ptr(mvStack.top()));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(pMat));

    glEnable(GL_CULL_FACE);		//调整深度
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    glBindVertexArray(Vao[0]);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, EarthTexture);
    glDrawArrays(GL_TRIANGLES, 0, mySphere.getNumIndices());
    glBindVertexArray(0);

    mvStack.pop();



}

int main()
{
    if (!glfwInit()) { exit(EXIT_FAILURE); }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    GLFWwindow* window = glfwCreateWindow(width, height, "地球公转与自转系统", NULL, NULL);

    //回调函数
    glfwSetKeyCallback(window, key_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);


    glfwMakeContextCurrent(window);
    if (glewInit() != GLEW_OK) { exit(EXIT_FAILURE); }
    //glfwSwapInterval(1);

    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    init(window);


    

    while (!glfwWindowShouldClose(window)) {

        display(window, glfwGetTime());
        glfwSwapBuffers(window);

        //获取事件流
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);

}
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    // 当用户按下ESC键,我们设置window窗口的WindowShouldClose属性为true
    // 关闭应用程序
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    //当用户按下J/j键，我们更改中心球体的纹理
    else if (key == GLFW_KEY_J && action == GLFW_PRESS)
    {
        SunTextureIndex++;
        SunTextureIndex %= SunTextureFileNum;
        creatTexture(SunTexture, SunTextureFileNames[SunTextureIndex]);
    }
    //当用户按下K/k键，我们更改边缘球体的纹理
    else if (key == GLFW_KEY_K && action == GLFW_PRESS)
    {
        EarthTextureIndex++;
        EarthTextureIndex %= EarthTextureFileNum;
        creatTexture(EarthTexture, EarthTextureFileNames[EarthTextureIndex]);
    }
    //空格复位摄像机
    else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    {
        camera = Camera(glm::vec3(0.0f, 0.0f, 4.0f));
    }
}
void window_size_callback(GLFWwindow* win, int newWidth, int newHeight) {
    //更新全局宽高
    glfwGetFramebufferSize(win, &width, &height);
    glViewport(0, 0, newWidth, newHeight);
    pMat = glm::perspective(glm::radians(camera.Zoom), (float)newWidth / (float)newHeight, 0.1f, 1000.0f);
}
//视角平移
void processInput(GLFWwindow* window)
{
    // 返回这个按键是否正在被按下
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)//是否按下了返回键
        glfwSetWindowShouldClose(window, true);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);
}
//视角转动
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }
    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
    lastX = xpos;
    lastY = ypos;
    camera.ProcessMouseMovement(xoffset, yoffset);
}
//视角缩放
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}






